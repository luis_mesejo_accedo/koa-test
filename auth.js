const got = require ('got');
const crypto = require ('crypto-js');

const nl = "\n";
const version = "ACCEDO-V1";
const contentType = "application/json";
const accessKeyId = "MYI4cUel0NZjoret8pfHRvSZ-McQHNPKyQv60D2yVl8";
const secretAccessKey = "kFr3v5/A4rfOaulw8Ot3Ew==";
const host = "https://management-api.one.accedo.tv";

function getSignature(method, uri, queryString, body) {
    const date = new Date ();
    const fullDateString = date.toISOString ();
    const shortDateString = date.toISOString ().split ('T')[0];
    const payloadHash = crypto.SHA256 (body ? body : "");
    const requestString = method + nl +
        uri + nl +
        queryString + nl +
        contentType + nl +
        host + nl +
        fullDateString + nl +
        payloadHash;
    // Generate signing key
    const signingKey = crypto.SHA256 (version + secretAccessKey + shortDateString);
    // Sign the request
    const signature = crypto.SHA256 (signingKey + requestString);
    const authorization = version + " Credential=" + accessKeyId + " Signature=" + signature;

    return {
        "content-type": contentType,
        "authorization": authorization,
        "x-accedo-date": fullDateString,
        "requestDate": fullDateString
    }
}

module.exports = {
    get: async function(uri, queryString, body) {
        return got.get (host + uri, {
            headers: getSignature ("GET", uri, queryString, body),
            json: {}
        }).then (function(response) {
            return response.body;
        }).catch (function(error) {
            console.log (error);
            return error.body;
        })

    }
};
