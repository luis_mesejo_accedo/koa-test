const Koa = require('koa');
const Router = require('koa-router');

const app = new Koa();

const auth = require('./auth');

// const router = new Router();
// router.get('/', (ctx, next) => {
//   // ctx.router available
// });
// app
//   .use(router.routes())
//   .use(router.allowedMethods());


app.use(async (ctx, next) => {
  const res = await auth.get('/profiles', "", "");
  ctx.body = res;
  //await next();
});

app.listen(3000);

